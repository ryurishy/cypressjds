import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given("open website portal jabar", ()=>{
    cy.visit('https://develop--portal-jabar-cms.netlify.app/login')
})

When("login with data account", ()=>{
    cy.get('#email').type('rekrutment1@gmail.com')
    cy.get('#password').type('jabarjuara')
    cy.get('[type=submit]').click()
})

Then("dashboard portal jabar appear",()=>{
    cy.wait(5000)
    cy.get('[data-cy="header__title"]').contains('Dashboard')
})

When("I click button Layanan", ()=>{
    cy.get('[title="Layanan"]').click()
})

And("I click button Tambah Layanan", ()=>{
    cy.wait(5000)
    cy.contains('Tambah Layanan').click({ force: true })
})

Then("I scroll page until Kategori Layanan",()=>{
    cy.wait(5000)
    // cy.scrollTo(0, 500)
    cy.get('textarea[placeholder="Masukkan deskripsi layanan"]').scrollIntoView()
})

And("I search Kategori Layanan with data {string}", (dataSearch)=>{
    cy.get('input[placeholder="Pilih kategori layanan SPBE"]').click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/div[2]/div[1]/section[1]/span[10]/div[1]/div[2]/div[1]/div[2]/div[1]/input[1]`).click()
    cy.wait(2000)
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/div[2]/div[1]/section[1]/span[10]/div[1]/div[2]/div[1]/div[2]/div[1]/input[1]`).type(dataSearch)
    
})

Then("I choose Kategori Layanan Pertahanan",()=>{
    cy.xpath(`//span[contains(text(),'Pertahanan')]`).click()
})

Then("I choose Teknis Layanan Online",()=>{
    cy.get('input[placeholder="Pilih teknis layanan"]').click()
    cy.xpath(`//span[contains(text(),'Online')]`).click()
})

Then("I scroll page until Tarif Layanan",()=>{
    cy.wait(3000)
    // cy.scrollTo(0, 500)
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/span[2]/div[1]/div[1]/input[1]`).scrollIntoView()
})

Then("I input Tarif Layanan {string} and Tarif Maksimal {string}",(min,max)=>{
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[3]/div[2]/div[2]/div[1]/label[1]/span[1]`).click()

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[3]/div[2]/div[1]/span[1]/div[1]/div[1]/input[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[3]/div[2]/div[1]/span[1]/div[1]/div[1]/input[1]`).type(min)

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[3]/div[2]/div[2]/span[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[3]/div[2]/div[2]/span[1]`).type(max)
})

Then("I scroll page until Waktu Operasional",()=>{
    cy.wait(3000)
    // cy.scrollTo(0, 500)
    cy.xpath(`//label[contains(text(),'Waktu Operasional')]`).scrollIntoView()
})

Then("I click all checkbox in Waktu Operasional field",()=>{
    // cy.scrollTo(0, 500)
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[5]/div[2]/div[1]/div[1]/div[1]/i[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[5]/div[3]/div[1]/div[1]/div[1]/i[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[5]/div[4]/div[1]/div[1]/div[1]/i[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[5]/div[5]/div[1]/div[1]/div[1]/i[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[5]/div[6]/div[1]/div[1]/div[1]/i[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[5]/div[7]/div[1]/div[1]/div[1]/i[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[5]/div[8]/div[1]/div[1]/div[1]/i[1]`).click()
})

Then("I input data in Lokasi Pelayanan field",()=>{
    cy.wait(3000)
    // cy.scrollTo(0, 500)
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[3]/div[2]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]`).click()
    cy.xpath(`//span[contains(text(),'Khusus')]`).click()

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[3]/div[2]/div[1]/section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[3]/div[2]/div[1]/section[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/input[1]`).type('UPTD')
    cy.xpath(`//span[contains(text(),'UPTD')]`).click()

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[3]/div[2]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/input[1]`).type('Ciburuy')

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[3]/div[2]/div[1]/section[1]/div[1]/div[4]/textarea[1]`).type('Bandung Barat')

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[3]/div[2]/div[1]/section[1]/div[1]/div[5]/div[1]/div[1]/input[1]`).type('08123456789')
})

Then("I click button Simpan dan Lanjutkan",()=>{
    cy.wait(3000)
    // cy.scrollTo(0, 500)
    cy.xpath(`//span[contains(text(),'Simpan & Lanjutkan')]`).click()
})

Then("I input form Aplikasi with Three Data",()=>{
    cy.wait(3000)
    // cy.scrollTo(0, 500)
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]`).click()
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/section[1]/div[1]/div[1]/div[2]/div[1]/ul[1]/li[1]/span[1]`).click()

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/section[1]/div[2]/div[1]/div[1]/input[1]`).type('Pelaporan')

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/section[2]/div[1]/div[1]/div[1]/div[1]/input[1]`).type('Membuat Pelaporan')

    cy.xpath(`//textarea[@id='application-feature']`).type('Pembuatan Pelaporan yang bagus')
    cy.xpath(`//span[contains(text(),'Tambahkan Fitur Aplikasi')]`).click()


    //data 2
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/section[2]/div[2]/div[1]/div[1]/div[1]/input[1]`).type('Membuat Input data')

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/section[2]/div[2]/div[2]/textarea[1]`).type('Pembuatan input data yang bagus')
    cy.xpath(`//span[contains(text(),'Tambahkan Fitur Aplikasi')]`).click()

    //data 3
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/section[2]/div[3]/div[1]/div[1]/div[1]/input[1]`).type('Membuat aplikasi sosial media')

    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/section[2]/div[3]/div[2]/textarea[1]`).type('Pembuatan sosial media yang bagus')
    
})


Then("I input form Penanggung Jawab",()=>{
    cy.wait(3000)
    // cy.scrollTo(0, 500)
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[1]/input[1]`).type('jono')
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/div[2]/div[1]/section[1]/div[2]/div[1]/div[1]/input[1]`).type('086712512645')
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[1]/div[2]/div[1]/section[1]/div[2]/div[1]/div[1]/input[1]`).type('jono@jds.com')
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/input[1]`).type('surjono')
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/span[1]/form[1]/section[2]/span[1]/fieldset[1]/section[2]/div[2]/div[1]/section[1]/div[1]/div[2]/div[1]/div[1]/input[1]`).type('https://facebook.com')
})

Then("I click button Simpan",()=>{
    cy.wait(3000)
    // cy.scrollTo(0, 500)
    cy.xpath(`//span[contains(text(),'Simpan')]`).click()
    cy.xpath(`//div[contains(text(),'Ya, tambahkan layanan')]`).click()
    cy.xpath(`//div[contains(text(),'Saya Mengerti')]`).click()
})


And("I validate data",()=>{
    cy.wait(3000)
    // cy.scrollTo(0, 500)
    cy.xpath(`//tbody/tr[1]/td[7]/div[1]/div[1]/button[1]`).click()
    cy.xpath(`//a[contains(text(),'Detail')]`).click()
    cy.wait(3000)
    //validasi data
    cy.xpath(`//td[contains(text(),'RAL.01.01 Pertahanan')]`).should('exist')
    cy.xpath(`//span[contains(text(),'online')]`).should('exist')
    cy.xpath(`//tbody/tr[3]/td[2]/p[1]`).should('exist')

    cy.xpath(`//tbody/tr[5]/td[2]/div[1]/span[1]`).should('exist')
    cy.xpath(`//tbody/tr[5]/td[2]/div[2]/span[1]`).should('exist')
    cy.xpath(`//tbody/tr[5]/td[2]/div[3]/span[1]`).should('exist')
    cy.xpath(`//tbody/tr[5]/td[2]/div[4]/span[1]`).should('exist')
    cy.xpath(`//tbody/tr[5]/td[2]/div[5]/span[1]`).should('exist')
    cy.xpath(`//tbody/tr[5]/td[2]/div[6]/span[1]`).should('exist')
    cy.xpath(`//tbody/tr[5]/td[2]/div[7]/span[1]`).should('exist')

    cy.xpath(`//td[contains(text(),'KHUSUS')]`).should('exist')
    cy.xpath(`//td[contains(text(),'UPTD')]`).should('exist')
    cy.xpath(`//td[contains(text(),'Ciburuy')]`).should('exist')
    cy.xpath(`//td[contains(text(),'Bandung Barat')]`).should('exist')
    cy.xpath(`//td[contains(text(),'08123456789')]`).should('exist')

    //delete data
    cy.xpath(`//body/div[@id='app']/div[1]/div[1]/div[1]/main[1]/section[1]/div[2]/div[1]/button[1]/div[1]`).click()
    cy.xpath(`//p[contains(text(),'Ya, saya yakin')]`).click()
    cy.xpath(`//div[contains(text(),'Saya Mengerti')]`).click()
})