Feature: Portal Jabar Automation Testing

  Scenario: Verification confirmation data
    Given open website portal jabar
    When login with data account
    Then dashboard portal jabar appear
    When I click button Layanan
    And I click button Tambah Layanan
    Then I scroll page until Kategori Layanan
    And I search Kategori Layanan with data 'Pertahanan'
    Then I choose Kategori Layanan Pertahanan
    Then I choose Teknis Layanan Online
    Then I scroll page until Tarif Layanan
    Then I input Tarif Layanan '7000' and Tarif Maksimal '100000'
    Then I scroll page until Waktu Operasional
    Then I click all checkbox in Waktu Operasional field
    Then I input data in Lokasi Pelayanan field
    Then I click button Simpan dan Lanjutkan
    Then I input form Aplikasi with Three Data
    Then I click button Simpan dan Lanjutkan
    Then I input form Penanggung Jawab
    Then I click button Simpan
    And I validate data
    
